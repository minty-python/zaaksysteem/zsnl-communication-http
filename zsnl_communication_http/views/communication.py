# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import logging
from .serializers import (
    case_entry_serializer,
    contact_moment_entry_serializer,
    contacts_entry_serializer,
    message_entry_serializer,
    thread_entry_serializer,
)
from datetime import datetime
from minty.cqrs import UserInfo
from minty.exceptions import ValidationError
from minty_pyramid.session_manager import protected_route
from pyramid.httpexceptions import HTTPBadRequest
from pyramid.request import Request
from webob import Response

logger = logging.getLogger(__name__)


def _is_pip_user(user_info):
    return user_info.permissions.get("pip_user", False)


@protected_route("gebruiker")
def search_contact(request: Request, user_info: UserInfo):
    """Search Contacts with keyword.

    :param request: request
    :type request: Request
    :param user_info: injected user info
    :type user_info: UserInfo
    :raises HTTPForbidden: no permission
    :raises HTTPNotFound: folder not found
    :return: response
    :rtype: dict
    """

    query_instance = request.get_query_instance(
        "zsnl_domains.communication", user_info.user_uuid
    )

    try:
        keyword = request.params["keyword"]
    except KeyError:
        raise HTTPBadRequest(json={"error": f"Missing parameter 'keyword'"})

    type_filter = request.params.get(
        "filter[type]", "employee,person,organization"
    ).split(",")

    contacts_result = query_instance.search_contact(
        keyword=keyword, type_filter=type_filter
    )

    search_link = {"self": {"href": f"{request.current_route_path()}"}}

    return {
        "data": [
            contacts_entry_serializer(entry) for entry in contacts_result
        ],
        "links": search_link,
    }


@protected_route("gebruiker", "pip_user")
def thread_list(request: Request, user_info: UserInfo):
    """Get a thread list based on type (contact or case) and by the respective uuid .

    :param request: request
    :type request: Request
    :param user_info: injected user info
    :type user_info: UserInfo
    :raises HTTPForbidden: no permission
    :raises HTTPNotFound: folder not found
    :return: response
    :rtype: dict
    """
    query_instance = request.get_query_instance(
        "zsnl_domains.communication", user_info.user_uuid, user_info
    )

    case_uuid = request.params.get("filter[case_uuid]", None)
    contact_uuid = request.params.get("filter[contact_uuid]", None)
    message_types = request.params.get("filter[message_types]", None)

    thread_list = query_instance.get_thread_list(
        case_uuid=case_uuid,
        contact_uuid=contact_uuid,
        message_types=message_types,
    )

    link = {"self": {"href": f"{request.current_route_path()}"}}

    is_pip_user = _is_pip_user(user_info)
    return {
        "data": [
            thread_entry_serializer(entry, is_pip_user)
            for entry in thread_list
        ],
        "links": link,
    }


@protected_route("gebruiker")
def create_contact_moment(request: Request, user_info: UserInfo):
    """Create contact_moment.

    :param request: request
    :type request: Request
    :param user_info: injected user info
    :type user_info: UserInfo
    :raises HTTPForbidden: no permission
    :raises HTTPNotFound: folder not found
    :return: response
    :rtype: dict
    """
    case_uuid = request.json_body.get("case_uuid")

    try:
        contact_moment_uuid = request.json_body["contact_moment_uuid"]
        thread_uuid = request.json_body["thread_uuid"]
        contact_uuid = request.json_body["contact_uuid"]
        channel = request.json_body["channel"]
        content = request.json_body["content"]
        direction = request.json_body["direction"]
    except KeyError as error:
        raise HTTPBadRequest(json={"error": f"Missing parameters '{error}'"})

    cmd = request.get_command_instance(
        "zsnl_domains.communication", user_info.user_uuid
    )

    command_params = {
        "contact_moment_uuid": contact_moment_uuid,
        "thread_uuid": thread_uuid,
        "case_uuid": case_uuid,
        "contact_uuid": contact_uuid,
        "channel": channel,
        "content": content,
        "direction": direction,
    }

    cmd.create_contact_moment(**command_params)

    return {"data": {"success": True}}


@protected_route("gebruiker")
def create_note(request: Request, user_info: UserInfo):
    """Create a note for a case or a contact.

    :param request: request
    :type request: Request
    :param user_info: injected user info
    :type user_info: UserInfo
    :raises HTTPForbidden: no permission
    :raises HTTPNotFound: folder not found
    :return: response
    :rtype: dict
    """
    contact_uuid = request.json_body.get("contact_uuid")
    case_uuid = request.json_body.get("case_uuid")

    try:
        note_uuid = request.json_body["note_uuid"]
        thread_uuid = request.json_body["thread_uuid"]
        content = request.json_body["content"]

    except KeyError as error:
        raise HTTPBadRequest(json={"error": f"Missing parameters '{error}'"})

    cmd = request.get_command_instance(
        "zsnl_domains.communication", user_info.user_uuid
    )

    command_params = {
        "note_uuid": note_uuid,
        "thread_uuid": thread_uuid,
        "content": content,
        "contact_uuid": contact_uuid,
        "case_uuid": case_uuid,
    }

    cmd.create_note(**command_params)

    return {"data": {"success": True}}


@protected_route("gebruiker", "pip_user")
def get_message_list(request: Request, user_info: UserInfo):
    """Get a message list for the associated thread_uuid.

    :param request: request
    :type request: Request
    :param user_info: injected user info
    :type user_info: UserInfo
    :raises HTTPForbidden: no permission
    :return: response
    :rtype: dict
    """

    query_instance = request.get_query_instance(
        "zsnl_domains.communication", user_info.user_uuid, user_info
    )

    try:
        thread_uuid = request.params["thread_uuid"]
    except KeyError as error:
        raise HTTPBadRequest(json={"error": f"Missing parameter {error}"})

    result = query_instance.get_message_list(thread_uuid=thread_uuid)

    link = {"self": {"href": f"{request.current_route_path()}"}}

    messages = [
        message_entry_serializer(message=entry) for entry in result["messages"]
    ]

    case = None
    is_pip_user = _is_pip_user(user_info)

    if result["case"]:
        case = {"data": case_entry_serializer(result["case"], is_pip_user)}

    return {
        "meta": {"api_version": 2},
        "data": messages,
        "links": link,
        "relationships": {"case": case},
    }


@protected_route("gebruiker")
def search_case(request: Request, user_info: UserInfo):
    """Search Cases with a search term and minimum permission level.

    :param request: request
    :type request: Request
    :param user_info: injected user info
    :type user_info: UserInfotest_search_case
    :raises HTTPForbidden: no permission
    :raises HTTPNotFound: folder not found
    :return: response
    :rtype: dict
    """

    query_instance = request.get_query_instance(
        "zsnl_domains.communication", user_info.user_uuid
    )

    limit = request.params.get("limit", None)
    case_status_filter = request.params.get("filter[status]", None)

    if limit:
        try:
            limit = int(limit)
        except ValueError:
            raise ValidationError(
                f"'limit' should be an integer.", "validation/limit_not_int"
            )

    if case_status_filter:
        case_status_filter = case_status_filter.split(",")

    try:
        search_term = request.params["search_term"]
        permission = request.params["minimum_permission"]
    except KeyError as error:
        raise HTTPBadRequest(json={"error": f"Missing parameter {error} "})

    cases_result = query_instance.search_cases(
        search_term=search_term,
        permission=permission,
        case_status_filter=case_status_filter,
        limit=limit,
    )

    search_link = {"self": {"href": f"{request.current_route_path()}"}}

    return {
        "data": [case_entry_serializer(entry) for entry in cases_result],
        "links": search_link,
    }


@protected_route("gebruiker", "pip_user")
def get_case_list_for_contact(request: Request, user_info: UserInfo):
    """Return a list of cases the current user has access to, that are requested
    by a specified contact.

    :param request: Pyramid request object
    :type request: Request
    :param user_info: User information from the framework
    :type user_info: UserInfo
    :raises HTTPForbidden: User does not have permission to retrieve a case
        list for the specified contact.
    :return: response
    :rtype: dict
    """

    query_instance = request.get_query_instance(
        "zsnl_domains.communication", user_info.user_uuid, user_info
    )

    try:
        contact_uuid = request.params["contact_uuid"]
    except KeyError as error:
        raise HTTPBadRequest(json={"error": f"Missing parameter {error}"})

    cases_result = query_instance.get_case_list_for_contact(
        contact_uuid=contact_uuid
    )

    search_link = {"self": {"href": f"{request.current_route_path()}"}}

    return {
        "data": [case_entry_serializer(entry) for entry in cases_result],
        "links": search_link,
    }


@protected_route("gebruiker", "pip_user")
def create_external_message(request: Request, user_info: UserInfo):
    """Create an external message.

    :param request: request
    :type request: Request
    :param user_info: injected user info
    :type user_info: UserInfo
    :raises HTTPForbidden: no permission
    :raises HTTPNotFound: folder not found
    :return: response
    :rtype: dict
    """
    try:
        external_message_uuid = request.json_body["message_uuid"]
        thread_uuid = request.json_body["thread_uuid"]
        case_uuid = request.json_body["case_uuid"]
        subject = request.json_body["subject"]
        content = request.json_body["content"]
        message_type = request.json_body["message_type"]
        attachments = request.json_body["attachments"]
    except KeyError as error:
        raise HTTPBadRequest(json={"error": f"Missing parameters '{error}'"})

    participants = request.json_body.get("participants", [])
    direction = request.json_body.get("direction", "unspecified")

    cmd = request.get_command_instance(
        "zsnl_domains.communication", user_info.user_uuid, user_info
    )

    command_params = {
        "external_message_uuid": external_message_uuid,
        "thread_uuid": thread_uuid,
        "subject": subject,
        "content": content,
        "message_type": message_type,
        "case_uuid": case_uuid,
        "attachments": attachments,
        "participants": participants,
        "direction": direction,
    }

    cmd.create_external_message(**command_params)

    return {"data": {"success": True}}


@protected_route("gebruiker")
def link_thread_to_case(request: Request, user_info: UserInfo):
    """Link a thread to a case.

    :param request: request
    :type request: Request
    :param user_info: injected user info
    :type user_info: UserInfo
    :raises HTTPForbidden: no permission
    :raises HTTPNotFound: folder not found
    :return: response
    :rtype: dict
    """
    try:
        case_uuid = request.json_body["case_uuid"]
        thread_uuid = request.json_body["thread_uuid"]
        external_message_type = request.json_body["type"]
    except KeyError as error:
        raise HTTPBadRequest(json={"error": f"Missing parameters '{error}'"})

    cmd = request.get_command_instance(
        "zsnl_domains.communication", user_info.user_uuid, user_info
    )

    command_params = {
        "case_uuid": case_uuid,
        "thread_uuid": thread_uuid,
        "external_message_type": external_message_type,
    }

    cmd.link_thread_to_case(**command_params)

    return {"data": {"success": True}}


@protected_route("gebruiker", "pip_user")
def download_attachment(request: Request, user_info: UserInfo):
    """Request a download url for given attachment uuid.

    Response will contain re-direct(302) and url."""
    try:
        attachment_uuid = request.params["id"]
    except KeyError as error:
        raise HTTPBadRequest(json={"error": f"Missing parameter {error}"})

    query_instance = request.get_query_instance(
        "zsnl_domains.communication", user_info.user_uuid, user_info
    )

    file_download_url = query_instance.get_download_link(
        attachment_uuid=attachment_uuid
    )

    return Response(status_int=302, location=file_download_url)


@protected_route("gebruiker")
def delete_message(request: Request, user_info: UserInfo):
    """Delete a message in a thread

    When a thread is empty , it will also be deleted.

    :param request: request
    :type request: Request
    :param user_info: injected user info
    :type user_info: UserInfo
    :raises HTTPForbidden: no permission
    :return: response
    :rtype: dict
    """
    try:
        message_uuid = request.json_body["message_uuid"]
    except KeyError as error:
        raise HTTPBadRequest(json={"error": f"Missing parameters '{error}'"})

    cmd = request.get_command_instance(
        "zsnl_domains.communication", user_info.user_uuid, user_info
    )
    cmd.delete_message(message_uuid=message_uuid)

    return {"data": {"success": True}}


@protected_route("gebruiker")
def import_email_message(request: Request, user_info: UserInfo):
    """Import email message of type `.msg`, `.mail` or `.eml`.

    :param request: request
    :type request: Request
    :param user_info: injected user info
    :type user_info: UserInfo
    :raises HTTPForbidden: no permission
    :return: response
    :rtype: dict
    """
    try:
        case_uuid = request.json_body["case_uuid"]
        file_uuid = request.json_body["file_uuid"]
    except KeyError as error:
        raise HTTPBadRequest(json={"error": f"Missing parameters '{error}'"})

    cmd = request.get_command_instance(
        "zsnl_domains.communication", user_info.user_uuid, user_info
    )
    cmd.process_email_file(file_uuid=file_uuid, case_uuid=case_uuid)

    return {"data": {"success": True}}


@protected_route("pip_user", "gebruiker")
def mark_messages_unread(request: Request, user_info: UserInfo):
    """
    Mark messages as unread by a given list of message UUID's
    :param request: Request
    :param user_info: UserInfo
    :return: Response
    :raises: HTTP Unauthorized/forbidden
    """
    try:
        message_uuids = request.json_body["message_uuids"]
        context = request.json_body["context"]
    except KeyError as error:
        raise HTTPBadRequest(json={"error": f"Missing parameters '{error}'"})

    cmd = request.get_command_instance(
        "zsnl_domains.communication", user_info.user_uuid, user_info
    )
    cmd.mark_messages_unread(message_uuids=message_uuids, context=context)

    return {"data": {"success": True}}


@protected_route("pip_user", "gebruiker")
def mark_messages_read(request: Request, user_info: UserInfo):
    """Mark messages as read.

    :param request: request
    :type request: Request
    :param user_info: injected user info
    :type user_info: UserInfo
    :raises HTTPForbidden: no permission
    :return: response
    :rtype: dict
    """
    try:
        message_uuids = request.json_body["message_uuids"]
        context = request.json_body["context"]
    except KeyError as error:
        raise HTTPBadRequest(json={"error": f"Missing parameters '{error}'"})

    cmd = request.get_command_instance(
        "zsnl_domains.communication", user_info.user_uuid, user_info
    )
    timestamp = datetime.now().isoformat()
    cmd.mark_messages_read(
        message_uuids=message_uuids, context=context, timestamp=timestamp
    )

    return {"data": {"success": True}}


@protected_route("gebruiker", "pip_user")
def preview_attachment(request: Request, user_info: UserInfo):
    """Request a preview url for given attachment uuid.

    Response will contain re-direct(302) and url."""
    try:
        attachment_uuid = request.params["id"]
    except KeyError as error:
        raise HTTPBadRequest(json={"error": f"Missing parameter {error}"})

    query_instance = request.get_query_instance(
        "zsnl_domains.communication", user_info.user_uuid, user_info
    )

    file_preview_url = query_instance.get_preview_link(
        attachment_uuid=attachment_uuid
    )

    return Response(status_int=302, location=file_preview_url)


@protected_route("gebruiker")
def get_contact_moment_list(request: Request, user_info: UserInfo):
    """Request list of all contact moments."""
    query_instance = request.get_query_instance(
        "zsnl_domains.communication", user_info.user_uuid, user_info
    )

    contact_moment_list = query_instance.get_contact_moment_list()

    return {
        "data": [
            contact_moment_entry_serializer(entry)
            for entry in contact_moment_list
        ],
    }
