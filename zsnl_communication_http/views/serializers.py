# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from zsnl_communication_http.views import constants
from zsnl_domains.communication.entities import AttachedFile, Contact, Thread


def contacts_entry_serializer(contact: Contact):
    """Contact entity serializer

    :param contact: Contact
    :return: serialized dictionary for contacts.
    :rtype dict
    """
    return {
        "id": str(contact.uuid),
        "type": "contact",
        "attributes": {
            "name": contact.name,
            "type": contact.type,
            "address": contact.address,
            "email_address": contact.email_address,
        },
    }


def thread_entry_serializer(thread: Thread, is_pip_user: bool = False):
    """Simple serializer for thread to create thread messages.

    :param thread: Thread entry.
    :return: A dict of the api spec for each thread message.
    :rtype dict
    """

    message_slug = ""
    if hasattr(thread.last_message_cache, "get"):
        message_slug = f"-{thread.last_message_cache.get('slug', '')}"

    summary = f"{str(thread.id)}-{thread.thread_type}{message_slug}"

    thread_entry = {
        "id": str(thread.uuid),
        "type": "message_thread",
        "meta": {
            "created": thread.created.isoformat(),
            "last_modified": thread.last_modified.isoformat(),
            "number_of_messages": thread.number_of_messages,
            "unread_employee_count": thread.unread_employee_count,
            "unread_pip_count": thread.unread_pip_count,
            "summary": summary,
            "attachment_count": thread.attachment_count,
        },
        "attributes": {
            "thread_type": thread.thread_type,
            "last_message": thread.last_message_cache,
        },
        "relationships": {},
        "links": {
            "thread_messages": {
                "href": f"/v2/communication/get_message_list?thread_uuid={thread.uuid}"
            }
        },
    }

    relationships = thread_entry["relationships"]
    if getattr(thread.case, "uuid", None):
        case = {"data": case_entry_serializer(thread.case, is_pip_user)}
        relationships["case"] = case

    if getattr(thread.contact, "uuid", None):
        contact = {"data": contacts_entry_serializer(thread.contact)}
        relationships["contact"] = contact

    return thread_entry


def message_entry_serializer(message):
    """Simple serializer for thread to create thread messages.

    :param message: Thread entry.
    :return: A dict of the api spec for each thread message.
    :rtype dict
    """

    if message.message_type == "note":
        serialized_msg = note_serializer(message)
    if message.message_type == "contact_moment":
        serialized_msg = contact_moment_serializer(message)
    if message.message_type == "external":
        serialized_msg = external_message_serializer(message)
    if message.message_type == "email":
        raise NotImplementedError("email not implemented yet")
    return serialized_msg


def contact_moment_serializer(message):

    return {
        "type": message.message_type,
        "id": str(message.uuid),
        "meta": {
            "last_modified": message.last_modified.isoformat(),
            "created": message.created_date.isoformat(),
            "message_date": message.message_date.isoformat(),
            "summary": message.message_slug,
        },
        "attributes": {
            "content": message.content,
            "direction": message.direction,
            "channel": message.channel,
        },
        "relationships": {
            "created_by": {
                "data": contacts_entry_serializer(message.created_by)
            },
            "recipient": {
                "data": contacts_entry_serializer(message.recipient)
            },
            "thread": {
                "data": slim_thread_entry_serializer(message.thread_uuid)
            },
        },
    }


def external_message_serializer(message):
    relationships = {
        "thread": {"data": slim_thread_entry_serializer(message.thread_uuid)},
        "attachments": {
            "data": [
                attachment_serializer(attachment=att)
                for att in message.attachments
            ]
        },
    }

    if message.created_by is not None:
        relationships["created_by"] = {
            "data": contacts_entry_serializer(message.created_by)
        }

    return {
        "type": "external_message",
        "id": str(message.uuid),
        "meta": {
            "last_modified": message.last_modified.isoformat(),
            "created": message.created_date.isoformat(),
            "message_date": message.message_date.isoformat(),
            "read_pip": message.read_pip.isoformat()
            if message.read_pip
            else None,
            "read_employee": message.read_employee.isoformat()
            if message.read_employee
            else None,
            "summary": message.message_slug,
            "attachment_count": message.attachment_count,
            "failure_reason": message.failure_reason,
        },
        "attributes": {
            "message_type": message.external_message_type,
            "content": message.content,
            "subject": message.subject,
            "participants": message.participants,
            "is_imported": message.is_imported,
        },
        "relationships": relationships,
    }


def note_serializer(message):

    return {
        "type": message.message_type,
        "id": str(message.uuid),
        "meta": {
            "last_modified": message.last_modified.isoformat(),
            "created": message.created_date.isoformat(),
            "message_date": message.message_date.isoformat(),
            "summary": message.message_slug,
        },
        "attributes": {"content": message.content},
        "relationships": {
            "created_by": {
                "data": contacts_entry_serializer(message.created_by)
            },
            "thread": {
                "data": slim_thread_entry_serializer(message.thread_uuid)
            },
        },
    }


def slim_thread_entry_serializer(thread_uuid):
    return {"id": str(thread_uuid), "type": "message_thread"}


def case_entry_serializer(case, is_pip_user: bool = False):
    if is_pip_user:
        description = case.description_public
    else:
        description = case.description

    return {
        "type": "case",
        "id": str(case.uuid),
        "attributes": {
            "display_id": case.id,
            "description": description,
            "case_type_name": case.case_type_name,
            "status": case.status,
        },
    }


def attachment_serializer(attachment: AttachedFile):
    """Serializer for AttachedFile entity."""
    links = {}

    links["download"] = {
        "href": f"/api/v2/communication/download_attachment?id={attachment.uuid}"
    }

    if (
        attachment.preview_uuid
        or attachment.mimetype == constants.PDF_MIMETYPE
        or attachment.mimetype in constants.IMAGE_MIMETYPES
    ):
        links["preview"] = {
            "href": f"/api/v2/communication/preview_attachment?id={attachment.uuid}"
        }
        if attachment.mimetype in constants.IMAGE_MIMETYPES:
            links["preview"]["meta"] = {"content-type": attachment.mimetype}
        else:
            links["preview"]["meta"] = {"content-type": "application/pdf"}

    serialized = {
        "type": "attachment",
        "id": str(attachment.uuid),
        "attributes": {
            "name": attachment.filename,
            "md5": attachment.md5,
            "size": attachment.size,
            "mimetype": attachment.mimetype,
            "date_created": attachment.date_created,
        },
        "links": links,
    }
    return serialized


def contact_moment_entry_serializer(contact_moment):
    """Serializer for contact_moment_overview entity."""
    result = {
        "type": "contact_moment_overview",
        "id": str(contact_moment.uuid),
        "attributes": {
            "contact_type": contact_moment.contact_type,
            "contact": contact_moment.contact,
            "contact_uuid": contact_moment.contact_uuid,
            "channel": contact_moment.channel,
            "summary": contact_moment.summary,
            "registration_date": contact_moment.created,
            "direction": contact_moment.direction,
            "case_id": contact_moment.case_id,
        },
    }
    return result
