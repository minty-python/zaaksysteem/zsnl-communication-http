# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import pytest
from collections import namedtuple
from datetime import datetime
from unittest import mock
from uuid import uuid4
from zsnl_communication_http.views import serializers
from zsnl_domains.communication.entities import (
    AttachedFile,
    Case,
    Contact,
    ContactMomentOverview,
    Thread,
)


class TestSerializers:
    def test_thread_entry_serializer(self):
        case_uuid = uuid4()
        subject_uuid = uuid4()
        thread_uuid = uuid4()
        last_modified = created_date = datetime.utcnow()
        summary = "1-note-note message text"

        case_id = 2
        case = Case(
            id=case_id,
            uuid=case_uuid,
            description="private",
            description_public="public",
            case_type_name=None,
            status="open",
        )
        subject = Contact(id=1, uuid=subject_uuid, name="admin", type=None)

        thread = Thread(
            uuid=thread_uuid,
            id=1,
            thread_type="note",
            created=created_date,
            last_modified=last_modified,
            last_message_cache={"slug": "note message text"},
            case=case,
            contact=subject,
            number_of_messages=10,
            unread_pip_count=10,
            unread_employee_count=0,
            attachment_count=2,
        )

        assert serializers.thread_entry_serializer(thread) == {
            "id": str(thread.uuid),
            "type": "message_thread",
            "meta": {
                "created": thread.created.isoformat(),
                "last_modified": thread.last_modified.isoformat(),
                "number_of_messages": 10,
                "unread_pip_count": 10,
                "unread_employee_count": 0,
                "summary": summary,
                "attachment_count": 2,
            },
            "attributes": {
                "thread_type": thread.thread_type,
                "last_message": thread.last_message_cache,
            },
            "relationships": {
                "case": {
                    "data": {
                        "type": "case",
                        "id": str(thread.case.uuid),
                        "attributes": {
                            "display_id": thread.case.id,
                            "description": "private",
                            "case_type_name": None,
                            "status": "open",
                        },
                    }
                },
                "contact": {
                    "data": {
                        "type": "contact",
                        "id": str(thread.contact.uuid),
                        "attributes": {
                            "name": thread.contact.name,
                            "type": None,
                            "address": None,
                            "email_address": None,
                        },
                    }
                },
            },
            "links": {
                "thread_messages": {
                    "href": f"/v2/communication/get_message_list?thread_uuid={thread_uuid}"
                }
            },
        }

    def test_contacts_entry(self):
        uuid1 = uuid4()
        value_to_format = Contact(
            id=123,
            uuid=uuid1,
            name="Test contact entry 1",
            type="contact_type",
            address="TEst test",
            email_address="email@example.com",
        )

        result = serializers.contacts_entry_serializer(value_to_format)
        assert result == {
            "id": str(uuid1),
            "type": "contact",
            "attributes": {
                "name": "Test contact entry 1",
                "type": "contact_type",
                "address": "TEst test",
                "email_address": "email@example.com",
            },
        }

        value_to_format = None
        with pytest.raises(AttributeError):
            serializers.contacts_entry_serializer(value_to_format)

    @mock.patch(
        "zsnl_communication_http.views.serializers.contact_moment_serializer"
    )
    @mock.patch("zsnl_communication_http.views.serializers.note_serializer")
    @mock.patch(
        "zsnl_communication_http.views.serializers.external_message_serializer"
    )
    def test_message_entry_serializer(
        self,
        external_message_serializer,
        note_serializer,
        contactmoment_serializer,
    ):
        note_serializer.return_value = {"note": "serialized"}
        contactmoment_serializer.return_value = {
            "contact_moment": "serialized"
        }
        external_message_serializer.return_value = {
            "external_message": "serialized"
        }
        message = namedtuple("message", "message_type")

        note = message(message_type="note")
        res = serializers.message_entry_serializer(message=note)
        res == {"note": "serialized"}

        cm = message(message_type="contact_moment")
        res = serializers.message_entry_serializer(message=cm)
        res == {"contact_moment": "serialized"}

        external_message = message(message_type="external")
        res = serializers.message_entry_serializer(message=external_message)
        res == {"external_message": "serialized"}

        with pytest.raises(NotImplementedError):
            email = message(message_type="email")
            res = serializers.message_entry_serializer(message=email)

    @mock.patch(
        "zsnl_communication_http.views.serializers.contacts_entry_serializer"
    )
    def test_contact_moment_serializer(self, mock_contacts):
        message = namedtuple(
            "message",
            """message_type uuid last_modified created_date created_by
         message_slug recipient thread_uuid content direction channel message_date""",
        )
        mock_contacts.return_value = {"type": "contact"}
        uuid = uuid4()
        thread_uuid = uuid4()

        now = datetime.utcnow()

        cm = message(
            message_type="contactmoment",
            uuid=uuid,
            last_modified=now,
            created_by="contact",
            created_date=now,
            message_date=now,
            message_slug="message slug",
            recipient="contact",
            thread_uuid=thread_uuid,
            content="some content",
            direction="incoming",
            channel="phone",
        )

        res = serializers.contact_moment_serializer(cm)

        assert res == {
            "attributes": {
                "channel": "phone",
                "content": "some content",
                "direction": "incoming",
            },
            "id": str(uuid),
            "type": "contactmoment",
            "meta": {
                "created": now.isoformat(),
                "last_modified": now.isoformat(),
                "message_date": now.isoformat(),
                "summary": "message slug",
            },
            "relationships": {
                "created_by": {"data": {"type": "contact"}},
                "recipient": {"data": {"type": "contact"}},
                "thread": {
                    "data": {"id": str(thread_uuid), "type": "message_thread"}
                },
            },
        }

    @mock.patch(
        "zsnl_communication_http.views.serializers.contacts_entry_serializer"
    )
    def test_note_serializer(self, mock_contacts):
        message = namedtuple(
            "message",
            """message_type uuid last_modified created_date created_by
         message_slug thread_uuid content message_date""",
        )
        mock_contacts.return_value = {"type": "contact"}
        uuid = uuid4()
        thread_uuid = uuid4()
        now = datetime.utcnow()
        note = message(
            message_type="note",
            uuid=uuid,
            last_modified=now,
            created_by="contact",
            created_date=now,
            message_date=now,
            message_slug="message slug",
            thread_uuid=thread_uuid,
            content="some content",
        )

        res = serializers.note_serializer(note)

        assert res == {
            "attributes": {"content": "some content"},
            "id": str(uuid),
            "type": "note",
            "meta": {
                "created": now.isoformat(),
                "last_modified": now.isoformat(),
                "message_date": now.isoformat(),
                "summary": "message slug",
            },
            "relationships": {
                "created_by": {"data": {"type": "contact"}},
                "thread": {
                    "data": {"id": str(thread_uuid), "type": "message_thread"}
                },
            },
        }

    @mock.patch(
        "zsnl_communication_http.views.serializers.contacts_entry_serializer"
    )
    def test_external_message_serializer(self, mock_contacts):
        message = namedtuple(
            "message",
            [
                "message_type",
                "uuid",
                "last_modified",
                "created_date",
                "message_date",
                "created_by",
                "message_slug",
                "thread_uuid",
                "content",
                "subject",
                "external_message_type",
                "attachments",
                "participants",
                "is_imported",
                "read_pip",
                "read_employee",
                "attachment_count",
                "failure_reason",
            ],
        )
        mock_contacts.return_value = {"type": "contact"}
        uuid = uuid4()
        thread_uuid = uuid4()

        now = datetime.utcnow()

        external_message = message(
            message_type="external",
            uuid=uuid,
            last_modified=now,
            created_by="contact",
            created_date=now,
            message_date=now,
            message_slug="message slug",
            thread_uuid=thread_uuid,
            content="some content",
            subject="subject",
            external_message_type="pip",
            attachments=[],
            participants=[
                {
                    "role": "cc",
                    "display_name": "display",
                    "address": "address@example.com",
                }
            ],
            is_imported=False,
            read_pip=now,
            read_employee=None,
            attachment_count=2,
            failure_reason="Some SMTP failure",
        )

        res = serializers.external_message_serializer(message=external_message)

        assert res == {
            "attributes": {
                "message_type": "pip",
                "subject": "subject",
                "content": "some content",
                "participants": [
                    {
                        "role": "cc",
                        "display_name": "display",
                        "address": "address@example.com",
                    }
                ],
                "is_imported": False,
            },
            "id": str(uuid),
            "type": "external_message",
            "meta": {
                "created": now.isoformat(),
                "last_modified": now.isoformat(),
                "message_date": now.isoformat(),
                "read_pip": now.isoformat(),
                "read_employee": None,
                "summary": "message slug",
                "attachment_count": 2,
                "failure_reason": "Some SMTP failure",
            },
            "relationships": {
                "created_by": {"data": {"type": "contact"}},
                "thread": {
                    "data": {"id": str(thread_uuid), "type": "message_thread"}
                },
                "attachments": {"data": []},
            },
        }

    def test_case_serializer(self):
        uuid = uuid4()
        case_type_search = "case"
        case_type_name = "case_search_name"
        description = "desc"
        description_public = "descpub"
        case = Case(
            uuid=uuid,
            id=1,
            description=description,
            description_public=description_public,
            case_type_name=case_type_name,
            status="open",
        )

        res = serializers.case_entry_serializer(case)

        assert res == {
            "type": case_type_search,
            "id": str(uuid),
            "attributes": {
                "display_id": 1,
                "description": description,
                "case_type_name": case_type_name,
                "status": "open",
            },
        }

    def test_case_serializer_pip(self):
        uuid = uuid4()
        case_type_search = "case"
        case_type_name = "case_search_name"
        description = "desc"
        description_public = "pubdesc"
        case = Case(
            uuid=uuid,
            id=1,
            description=description,
            description_public=description_public,
            case_type_name=case_type_name,
            status="open",
        )

        res = serializers.case_entry_serializer(case=case, is_pip_user=True)

        assert res == {
            "type": case_type_search,
            "id": str(uuid),
            "attributes": {
                "display_id": 1,
                "description": description_public,
                "case_type_name": case_type_name,
                "status": "open",
            },
        }

    def test_attachment_serializer(self):
        file = AttachedFile(
            uuid=uuid4(),
            filename="test.pdf",
            md5="5c7133a947bbd823719db12620fda5c9",
            size=4532,
            mimetype="application/pdf",
            date_created="2019-09-19T15:45:41.423244",
            storage_location="S3Local",
        )
        serialized = serializers.attachment_serializer(attachment=file)
        assert serialized["type"] == "attachment"
        assert serialized["id"] == str(file.uuid)
        assert serialized["attributes"]["name"] == file.filename
        assert serialized["attributes"]["size"] == file.size
        assert serialized["attributes"]["md5"] == file.md5
        assert serialized["attributes"]["mimetype"] == file.mimetype
        assert serialized["attributes"]["date_created"] == file.date_created

        assert (
            serialized["links"]["download"]["href"]
            == f"/api/v2/communication/download_attachment?id={file.uuid}"
        )

        assert (
            serialized["links"]["preview"]["href"]
            == f"/api/v2/communication/preview_attachment?id={file.uuid}"
        )
        assert serialized["links"]["preview"]["meta"] == {
            "content-type": "application/pdf"
        }

    def test_attachment_serializer_image(self):
        file = AttachedFile(
            uuid=uuid4(),
            filename="test_img.png",
            md5="5c7133a947bbd823719db12620fda5c9",
            size=4532,
            mimetype="image/png",
            date_created="2019-09-19T15:45:41.423244",
            storage_location="S3Local",
        )
        serialized = serializers.attachment_serializer(attachment=file)
        assert serialized["type"] == "attachment"
        assert serialized["id"] == str(file.uuid)
        assert serialized["attributes"]["name"] == file.filename
        assert serialized["attributes"]["size"] == file.size
        assert serialized["attributes"]["md5"] == file.md5
        assert serialized["attributes"]["mimetype"] == file.mimetype
        assert serialized["attributes"]["date_created"] == file.date_created

        assert (
            serialized["links"]["download"]["href"]
            == f"/api/v2/communication/download_attachment?id={file.uuid}"
        )

        assert (
            serialized["links"]["preview"]["href"]
            == f"/api/v2/communication/preview_attachment?id={file.uuid}"
        )
        assert serialized["links"]["preview"]["meta"] == {
            "content-type": "image/png"
        }

    def test_contact_moment_entry_serializer(self):
        contact_moment = ContactMomentOverview(
            contact_uuid=uuid4(),
            contact="testOrg",
            case_id=20,
            direction="outgoing",
            uuid=uuid4(),
            created="2020-12-06T14:16:42.133766+00:00",
            summary="Test organization.",
            channel="mail",
            thread_uuid=uuid4(),
            contact_type="organization",
        )
        serialized = serializers.contact_moment_entry_serializer(
            contact_moment
        )
        assert serialized["type"] == "contact_moment_overview"
        assert serialized["id"] == str(contact_moment.uuid)
        assert (
            serialized["attributes"]["contact_type"]
            == contact_moment.contact_type
        )
        assert serialized["attributes"]["contact"] == contact_moment.contact
        assert (
            serialized["attributes"]["contact_uuid"]
            == contact_moment.contact_uuid
        )
        assert serialized["attributes"]["channel"] == contact_moment.channel
        assert serialized["attributes"]["summary"] == contact_moment.summary
        assert (
            serialized["attributes"]["registration_date"]
            == contact_moment.created
        )
        assert (
            serialized["attributes"]["direction"] == contact_moment.direction
        )
        assert serialized["attributes"]["case_id"] == contact_moment.case_id
